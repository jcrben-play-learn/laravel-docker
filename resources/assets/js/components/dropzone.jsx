import React from 'react';
import Dropzone from 'react-dropzone';
import axios from 'axios';

// TODO: 413 error on large files
// NOTE: adapted from the library docs
class Accept extends React.Component {
  constructor() {
    super();
    this.state = {
      accepted: [],
      rejected: []
    };
  }

  onClickUploadFiles() {
    const formData = new FormData();
    formData.append(this.state.accepted[0].name, this.state.accepted[0]);
    axios.post('/upload', formData).then(() => {
      // TODO: confirmation toast
      this.setState({
          accepted: []
      });
    });
  }

  render() {
    return (
      <section className="upload">
          <button onClick={() => this.onClickUploadFiles()}>Click to upload</button>
        <div className="dropzone">
          <Dropzone
            accept="image/jpeg, image/png, application/pdf"
            onDrop={(accepted, rejected) => { this.setState({ accepted, rejected }); }}
          >
            <p>Try dropping some files here, or click to select files to upload.</p>
            <p>Only *.jpeg, *.png, and *.pdf files will be accepted</p>
          </Dropzone>
        </div>
        <aside>
          <h2>Accepted files</h2>
          <ul>
            {
              this.state.accepted.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
            }
          </ul>
          <h2>Rejected files</h2>
          <ul>
            {
              this.state.rejected.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
            }
          </ul>
        </aside>
      </section>
    );
  }
}

export default Accept;
