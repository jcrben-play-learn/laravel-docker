import React from 'react';
import axios from 'axios';

class ListFiles extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            files: []
        };
    }

    // TODO: consider using this for preview
    static onClickDownload(filePath) {
        const fileName = filePath.slice('kraken/'.length);
        axios.get(`/file/${fileName}`, {
            responseType: 'blob'
        }).then((res) => {
            // per https://stackoverflow.com/a/42274086/4200039
            const url = window.URL.createObjectURL(res.data);
            const a = document.createElement('a');
            a.href = url;
            // TODO: this creates a duplicate extension
            a.download = fileName;
            a.click();
        });
    }

    onClickDelete(filePath, index) {
        const fileName = filePath.slice('kraken/'.length);
        axios.delete(`/file/${fileName}`).then(() => {
            const xs = this.state.files;
            const xsNew = [...xs.slice(0, index), ...xs.slice(index + 1)];
            this.setState({
                files: xsNew
            });
        });
    }

    componentDidMount() {
        axios.get('/files').then(res => {
            this.setState({
                loading: false,
                files: res.data
            });
        });
    }

    render() {
        return this.state.loading ? null :
            <section className="list">
                <ul>
                {this.state.files.map( (filePath, index) =>
                    <li key={filePath}>name: {filePath}
                        <button
                            onClick={() => ListFiles.onClickDownload(filePath)}
                        >
                            Download
                        </button>
                        <button
                            onClick={() => this.onClickDelete(filePath, index)}
                        >
                            Delete
                        </button>
                    </li>)
                }
                </ul>
            </section>
    }
}

export default ListFiles;