
// per https://stackoverflow.com/a/40222691/4200039, pulls devDependencies into build
require('./bootstrap');

import React from 'react';
import ReactDOM from 'react-dom';
import Dropzone from './components/dropzone';
import ListFiles from './components/listFiles';

const container = document.querySelector('#app');

ReactDOM.render(<React.Fragment>
    <Dropzone />
    <ListFiles />
</React.Fragment>, container);