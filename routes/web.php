<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;


// NOTE: in general, let failures propagate as exceptions

Route::get('/', function () {
    return view('home');
});

// https://laravel.com/docs/5.5/filesystem#file-uploads
Route::post('/upload', function (Request $request) {

    // TODO: consider how to handle duplicate named files
    foreach ($request->files->all() as $filename => $file) {
        $request->file($filename)->storeAs('kraken', $filename);
    }

    return response('success', 200)
                      ->header('Content-Type', 'text/plain');
});

Route::delete('/file/{name}', function ($name) {
    Storage::disk('local')->delete("kraken/{$name}");
});

Route::get('/file/{name}', function ($name) {
    return response()->download(storage_path("app/kraken/{$name}"));
});

Route::get('/files', function () {
    // https://laravel.com/docs/5.5/filesystem#directories
    $files = Storage::allFiles('kraken');
    return response()->json($files);
});
